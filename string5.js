function arrayToString(array){
    if(!Array.isArray(array)){
        throw 'Input is not an array';
    }
    let string = array.join(' ');
    const regex = /\s+/g;
    string = string.replaceAll(regex, ' ');
    return string.trim();
}

module.exports = { arrayToString };
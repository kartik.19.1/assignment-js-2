function ipToNumberArray(string){
    if(typeof(string) !== 'string'){
        return [];
    }
    const reg = new RegExp('^([0-9]{1,3}[.]){3}[0-9]{1,3}$');
    if(!reg.test(string)){
        return [];
    }
    let array = string.split('.');  
    for(let i = 0; i < array.length; i++){
        let num = Number(array[i]);
        if(num >= 0 && num <= 255){
            array[i] =  num;
        }
        else {
            return [];
        }
    }
    return array;
}

module.exports = { ipToNumberArray };
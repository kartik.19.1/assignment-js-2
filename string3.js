function getMonth(string){
    if(typeof(string) !== 'string'){
        throw 'Input is not a string';
    }
    const reg = new RegExp('^([0-9]{1,2}/){2}[0-9]{4}$');
    if(!reg.test(string)){
        throw 'Wrong date pattern in input';
    }
    const mm = Number(string.split('/')[0]);
    const months = ['','January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    if(mm >= 1 && mm <= 12){
        return months[mm];
    }
    else{
        throw 'Wrong month in input';
    }
}

module.exports = { getMonth };
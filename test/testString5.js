const string5 = require('../string5');


let array = ["the", "quick", "brown", "fox"];
let string; 
try{
    string = string5.arrayToString(array);
    console.log(string);
}
catch(e){
    console.error(e);
}


try{
    array = ["the", "quick", 235, "fox"];
    string = string5.arrayToString(array);
    console.log(string);
}
catch(e){
    console.error(e);
}


try{
    array = ["  the", null, 235, "fox", undefined, "jumb  "];
    string = string5.arrayToString(array);
    console.log(string);
}
catch(e){
    console.error(e);
}


try{
    string = string5.arrayToString();
    console.log(string);
}
catch(e){
    console.error(e);
}


try{
    string = string5.arrayToString('asdfgh');
    console.log(string);
}
catch(e){
    console.error(e);
}


try{
    array = ["the", 235, "fox", undefined];
    string = string5.arrayToString(array);
    console.log(string);
}
catch(e){
    console.error(e);
}


try{
    string = string5.arrayToString([]);
    console.log(string);
}
catch(e){
    console.error(e);
}

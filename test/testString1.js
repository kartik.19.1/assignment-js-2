const string1 = require('../string1');

let string = "$100.45";
let number = string1.stringToNumber(string);
console.log(number);

string = "$1,002.22";
number = string1.stringToNumber(string);
console.log(number);

string = "-$123";
number = string1.stringToNumber(string);
console.log(number);

string = "-123.6789056";
number = string1.stringToNumber(string);
console.log(number);

number = string1.stringToNumber();
console.log(number);

string = 1234;
number = string1.stringToNumber(string);
console.log(number);

string = '  ';
number = string1.stringToNumber(string);
console.log(number);

const string4 = require('../string4');


let obj = {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"};
let fname;
try{
    fname = string4.fullName(obj);
    console.log(fname);
}
catch(e){
    console.error(e);
}


try{
    obj = {"first_name": "JoHN", "last_name": "SMith"};
    fname = string4.fullName(obj);
    console.log(fname);
}
catch(e){
    console.error(e);
}


try{
    obj = {"first_name": "   ", "middle_name": "    doe    "};
    fname = string4.fullName(obj);
    console.log(fname);
}
catch(e){
    console.error(e);
}


try{
    obj = {"middle_name": "doe"};
    fname = string4.fullName(obj);
    console.log(fname);
}
catch(e){
    console.error(e);
}


try{
    obj = {"first_name": 23, "middle_name": "doe"};
    fname = string4.fullName(obj);
    console.log(fname);
}
catch(e){
    console.error(e);
}


try{
    fname = string4.fullName({});
    console.log(fname);
}
catch(e){
    console.error(e);
}


try{
    fname = string4.fullName('hello world');
    console.log(fname);
}
catch(e){
    console.error(e);
}


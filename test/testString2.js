const string2 = require('../string2');

let string = "111.139.161.143";
let array = string2.ipToNumberArray(string);
console.log(array);

string = "0.0.0.0";
array = string2.ipToNumberArray(string);
console.log(array);

string = "111.00.0 . ";
array = string2.ipToNumberArray(string);
console.log(array);

string = "111.12.26.10.34";
array = string2.ipToNumberArray(string);
console.log(array);

string = "111.12.26";
array = string2.ipToNumberArray(string);
console.log(array);

string = "100.09 .10.20";
array = string2.ipToNumberArray(string);
console.log(array);

string = "11,1.0.0.0";
array = string2.ipToNumberArray(string);
console.log(array);

string = "111.0;0.0";
array = string2.ipToNumberArray(string);
console.log(array);

string = "hello world";
array = string2.ipToNumberArray(string);
console.log(array);

string = 1234;
array = string2.ipToNumberArray(string);
console.log(array);

array = string2.ipToNumberArray();
console.log(array);